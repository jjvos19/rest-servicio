package bo.com.fie.cognos.rest.services;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

/**
 * Configura 
 * @author JAVA
 *
 */
public class AppConfig extends Application {

	private Set<Object> singleton = new HashSet<>();

	public AppConfig() {
		singleton.add(new PersonaService());
	}

	@Override
	public Set<Object> getSingletons() {
		return singleton;
	}
}
