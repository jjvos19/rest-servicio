package bo.com.fie.cognos.rest.services;

import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.media.jfxmedia.Media;

import bo.com.fie.cognos.rest.entidades.Persona;

@Path("/personas")
public class PersonaService {

	@GET
	@Path("/saludo")
//	@Produces(v	alue =MimeType )
	public String saludar() {
		return "hola mundo";
	}

	@GET
	@Path("/obtenerPersona/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String obtenerPersona(@PathParam("id") int id) {
//		int id = 24;
		Persona p = new Persona(id);
		return p.toString();
	}
}
